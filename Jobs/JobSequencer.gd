extends VBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func MoveUp(boardItem : Control):
	var ind = boardItem.get_index()
	move_child(boardItem, ind - 1)
	CheckIfDropIsBeforePick(boardItem)
	OnBoardChange()

func MoveDown(boardItem : Control):
	var ind = boardItem.get_index()
	move_child(boardItem, ind + 1)
	CheckIfDropIsBeforePick(boardItem)
	OnBoardChange()

func MoveTop(boardItem : Control):
	move_child(boardItem, 0)
	CheckIfDropIsBeforePick(boardItem)	
	OnBoardChange()

func MoveBottom(boardItem : Control):
	var num = get_child_count()
	move_child(boardItem, num )
	CheckIfDropIsBeforePick(boardItem)
	OnBoardChange()


func CheckIfDropIsBeforePick(movedBoardItem):
	if !("isPick") in movedBoardItem:
		return
	
	var pick = null
	var drop = null
	var _id = movedBoardItem.job.id
	var boardItems = get_children()

	for b in boardItems:
		if b.job.id == _id:
			if b.isPick:
				pick = b
			if !b.isPick:
				drop = b

	if pick == null || drop == null:
		return
				

	var pickIndex = pick.get_index()	
	var dropIndex = drop.get_index()


	#edge case fix 
	if dropIndex == 0 && pickIndex == 1:
		move_child(pick,0)
		move_child(drop,1)
	elif dropIndex < pickIndex:
		move_child(drop,pickIndex +1)
		#play sound



func OnBoardChange():
	get_parent().OnBoardChange()

	
		
	
