extends Node



var jobOrigins = []
var jobDestinations = []

var jobRef : String =  "res://Jobs/JobData.gd"

var idIndex = 0

#var jobValues = [10, 20, 50, 100]
#var weightModifier = 2

var jobTypeWeightsMorning = [0,0,0,1,1,1,1,2,2,0,3]
var jobTypeWeightsAfternoon = [1,1,1,1,2,2,0,3]



var monAMJobs = [0,0,0,0,1,1,1,2]
var monPMJobs = [1,1,1,1,1,2,2,0,3]

var tueAMJobs = [0,0,0,0,1,1,1,2,2]
var tuePMJobs = [1,1,1,2,2,0,3]

var wedAMJobs = [0,0,0,0,1,1,1,2,2,3]
var wedPMJobs = [1,1,1,1,2,2,2,3]

var thurAMJobs = [0,0,0,1,1,1,1,2,2,3]
var thurPMJobs = [1,1,1,2,2,2,2,3]

var friAMJobs = [0,0,1,1,1,2,2,2,2,3,3]
var friPMJobs = [1,1,2,2,2,3,3,3]


var jobsAM = [monAMJobs, tueAMJobs, wedAMJobs, thurAMJobs, friAMJobs]
var jobsPM = [monPMJobs, tuePMJobs, wedPMJobs, thurPMJobs, friPMJobs]

var jobWeightRange = [ Vector2(1,2), Vector2(1,10), Vector2(1,10), Vector2(1,10) ]
#{ 0 : "ALL DAY", 1 : "STANDARD", 2 : "EXPRESS", 3 : "PRIORITY RUSH"}


func _ready():
	randomize()
	Global.jobGenerator = self


func AddOrigin(dest):
	jobOrigins.push_back(dest)


func AddDestination(dest):
	jobDestinations.push_back(dest)


func GetNewJob():

	idIndex += 1

	var job : JobData = load(jobRef).new()

	var start = Utility.Choose(jobOrigins)
	var end = Utility.Choose(jobDestinations)

	if start == null || end == null:
		print("SOMETHINGWENT WRONG IN JOB CREATION")

	job.origin = start
	job.destination = end
	
	job.type = ChooseJobType()
	job.id = idIndex
	job.weight = ChooseJobWeight(job.type)
	job.dueTime = GetJobDueTime(job.type)


	return job


func GetJobDueTime(jobType) -> int:
	#{ 0 : "ALL DAY", 1 : "STANDARD", 2 : "EXPRESS", 3 : "PRIORITY RUSH"}
	var dueT = -1
	match(jobType):
		(0): #All Day
			dueT= Global.clock.GetDayEndInDueTime()
			
			
		(1): #Standard
			dueT=Global.clock.GetDueTime(Global.currentHour + 4, Global.currentMinute)

	

		(2): #Express
			dueT= Global.clock.GetDueTime(Global.currentHour + 2, Global.currentMinute + 30)

		(3): #Priority Rush
			dueT= Global.clock.GetDueTime(Global.currentHour + 1, Global.currentMinute + 15)

	if dueT > Global.clock.GetDayEndInDueTime():
		dueT = Global.clock.GetDayEndInDueTime()
	return dueT

func ChooseJobWeight(jobType :int):
	var _range = jobWeightRange[jobType]
	var wt = round(rand_range(_range.x,_range.y))
	return wt

#func CalculateJobValue(jobType : int, jobWeight : int):
#		var val = jobValues[jobType] + (jobWeight * weightModifier)
#		return val


func ChooseJobType():
	var _arr
	if Global.clock.IsBeforeNoon():
		_arr = jobsAM[Global.main.activeGameData.currentDay]
		print("AM JOB")
	else:
		_arr = jobsPM[Global.main.activeGameData.currentDay]
		print("PM JOB")

	return Utility.Choose(_arr)
	
