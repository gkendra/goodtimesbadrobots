extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var activeJobs = []




var jobGenerator : Node

var jobTimer : Timer 

var jobTimes = [9,8,7,6,5]


var maxJobs = 5
var currentJobs = 0

var chanceOfGettingJob = .8

func _ready():
	Global.jobManager = self
	jobGenerator = $JobGenerator
	InitJobTimer()
	var _err = SignalBus.connect("PAUSE_ON",self,"PauseOn")
	_err = SignalBus.connect("PAUSE_OFF",self,"PauseOff")

	

func PauseOn():
	jobTimer.paused = true
	$FirstJobTimer.paused = true

func PauseOff():
	jobTimer.paused = false
	$FirstJobTimer.paused = false

func InitJobTimer():
	jobTimer = Timer.new()
	add_child(jobTimer)
	jobTimer.autostart = true
	jobTimer.one_shot = false
	jobTimer.wait_time = jobTimes[Global.main.activeGameData.currentDay]
	jobTimer.start()
	var _err = jobTimer.connect("timeout",self, "JobTimerTimeOut")

func GetNewJob():
	Global.sfxPlayer.PlayNotificationSound()
	var newJob : JobData = jobGenerator.GetNewJob()
	#activeJobs.append(newJob)
	var managers = get_tree().get_nodes_in_group("JobManager")
	for m in managers:
		
		if m.has_method("AddNewJob"):
			m.AddNewJob(newJob)

			
func JobTimerTimeOut():
	var odds = randf()
	if odds < chanceOfGettingJob:
			
		GetNewJob()


func JobIsHovered(job : JobData, isCourier, isPick):
	var managers = get_tree().get_nodes_in_group("JobManager")
	for m in managers:
		
		if m.has_method("SetHoveredJob"):
			m.SetHoveredJob(job,isCourier,isPick)


func FirstJobTimeOut():
	GetNewJob()
