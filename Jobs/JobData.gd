extends Reference
class_name JobData 






var origin : Node2D
var destination : Node2D
var weight : int
var type : int
var courier = null
var id : int 
var dueTime : int

var isLate : bool = false
var isComplete : bool = false

#var moneyValue : float 

var jobTypesText = { 0 : "ALL DAY", 1 : "STANDARD", 2 : "EXPRESS", 3 : "PRIORITY RUSH"}
