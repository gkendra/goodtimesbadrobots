extends Panel



var unassignedBoard 
var courierBoards = []

export (NodePath) var courierBoardParentPath


export var unassignedBoardName : String = "New"

signal BOARD_CHANGED


var jobBoardSceneRef = "res://Jobs/JobContainer.tscn"

func _ready():
	Global.jobBoardManager = self
	
	call_deferred("InitJobBoards")

func InitJobBoards():
	unassignedBoard = load(jobBoardSceneRef).instance()
	$VBoxContainer/UnassignedTabs.add_child(unassignedBoard)
	unassignedBoard.SetDisplayName(unassignedBoardName)
	unassignedBoard.id = -1
	unassignedBoard.connect("BOARD_CHANGED", self, "JobBoardChanged")
	unassignedBoard.isCourier = false
	
	CreateCourierTabs()


func CreateCourierTabs():
	#var par = get_node(courierBoardParentPath)
	var par = $VBoxContainer/UnassignedTabs
	var couriers = Global.main.activeGameData.couriers
	for c in couriers:
		var newBoard = load(jobBoardSceneRef).instance()
		par.add_child(newBoard)
		newBoard.isCourier = true
		newBoard.SetDisplayName(c.displayName)
		courierBoards.append(newBoard)
		newBoard.id = c.id
		newBoard.connect("BOARD_CHANGED", self, "JobBoardChanged")
	

func AddNewJob(jobData):
	unassignedBoard.AddNewJob(jobData)


func MoveJob(boardItem : Control,newBoardID : int ):
	var job = boardItem.job
	print("new board ID:" + str(newBoardID))
	RemoveAllJobsByID(job.id)

	if newBoardID == 999: 
		unassignedBoard.AddNewJob(job)

	else:
		for c in courierBoards:
			if c.id == newBoardID:
				c.AddNewJob(job)

	


func RemoveAllJobsByID(id):
	unassignedBoard.RemoveJobByID(id)
	
	for c in courierBoards:
		c.RemoveJobByID(id)
	

func GetNextCourierJobLocation(courierID):
	for c in courierBoards:
		if c.id == courierID:
			return(c.GetNextJobLocation())

	assert(true != true, "unreachable code block")
	return null


func MarkJobAsComplete(courierID, jobLocation):
	for b in courierBoards:
		if b.id == courierID:
			b.MarkJobAsComplete(jobLocation)



func JobBoardChanged(boardID): 


	#child boards signal up to here, this broadcasts out to whoever
	emit_signal("BOARD_CHANGED", boardID)
