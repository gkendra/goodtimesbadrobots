extends Control

var job : JobData = null


var originPrefix  : String = "From: "
var destPrefix    : String = "To: "
var weightSuffix  : String = " LBS"
var jobIDPrefix   : String = "#"
var duePrefix     : String = "DUE: "

export(NodePath) var jobIDPath
export(NodePath) var destinationPath
export(NodePath) var originPath
export(NodePath) var jobTypePath
export(NodePath) var weightPath


export(NodePath) var buttonUpPath
export(NodePath) var buttonDownPath
export(NodePath) var buttonTopPath
export(NodePath) var buttonBottomPath

export(NodePath) var assignmentDropList

export (NodePath) var locationPath
export (NodePath) var pickDropPath

export (NodePath) var dueTimePath
export(NodePath) var latePath


func _ready():
	var _err = get_node(buttonUpPath).connect("pressed",self,"ButtonUpPressed")
	_err = get_node(buttonDownPath).connect("pressed",self,"ButtonDownPressed")
	_err = get_node(buttonTopPath).connect("pressed",self,"ButtonTopPressed")
	_err = get_node(buttonBottomPath).connect("pressed",self,"ButtonBottomPressed")
	_err = get_node(assignmentDropList).connect("REASSIGN",self, "ReassignRequested")
	
	_err = SignalBus.connect("CURRENT_TIME",self, "CheckForLateJob")
	
	var parID = get_parent().get_parent().id
	get_node(assignmentDropList).InitDropList(parID)

func Init(newJob) -> void:
	job = newJob
	assert(job != null, "NO job on jobboardItem")
	Update()	

func Update() -> void:
	if job == null : 
		return
	
	SetPanelColor(job.type)

	if  get_node_or_null(jobTypePath) != null:	
		get_node(jobTypePath).text = job.jobTypesText[job.type]
	
	if  get_node_or_null(originPath) != null:
		get_node(originPath).text = originPrefix + job.origin.displayName

	if  get_node_or_null(destinationPath) != null:
		get_node(destinationPath).text = destPrefix + job.destination.displayName
	
	if  get_node_or_null(weightPath) != null :
		get_node(weightPath).text = str(job.weight) + weightSuffix 

	if get_node_or_null(jobIDPath) != null:
		get_node(jobIDPath).text = jobIDPrefix + str(job.id)


	if get_node_or_null(dueTimePath) != null:
		if job.isLate:
			get_node(dueTimePath).text = "LATE"
		else:
			get_node(dueTimePath).text = duePrefix + Global.clock.GetStringTimeFromDueTime(job.dueTime)




func SetPanelColor(jobType : int):
	match jobType:
		(0):
			$Panel.self_modulate = Global.allDayColor
		(1):
			$Panel.self_modulate = Global.standardColor
		(2):
			$Panel.self_modulate = Global.expressColor
		(3):
			$Panel.self_modulate = Global.priorityRushColor
	


func ButtonUpPressed():
	get_parent().MoveUp(self)

func ButtonDownPressed():
	get_parent().MoveDown(self)

func ButtonTopPressed():
	get_parent().MoveTop(self)

func ButtonBottomPressed():
	get_parent().MoveBottom(self)


func ReassignRequested(assignID : int):
	if assignID == -2: #-2 is ID used for non function "Assign" droplist item
		return

	Global.jobBoardManager.MoveJob(self,assignID)

func CheckForLateJob(currentTime):
	if currentTime > job.dueTime:
		job.isLate = true
		Update()

func RemoveAssignmentDroplist():
	get_node(assignmentDropList).SetDisabled()


