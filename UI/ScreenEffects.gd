extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var _err = SignalBus.connect("PRE_END_OF_DAY", self, "FadeToBlack")
	show()
	FadeToClear()

func FadeToClear():
	SetDayText()
	$AnimationPlayer.play("FadeToClear")
	
func SetDayText():
	var currentDay = Global.main.activeGameData.currentDay

	match(currentDay):
		(0):
			$DayLabel.text = "MONDAY"
		(1):
			$DayLabel.text = "TUESDAY"
		(2):
			$DayLabel.text = "WEDNESDAY"
		(3):
			$DayLabel.text = "THURSDAY"
		(4):
			$DayLabel.text = "FRIDAY"


func ShowDayText():
	pass

func FadeToBlack():
	$AnimationPlayer.play("FadeToBlack")
