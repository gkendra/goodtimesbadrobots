extends Button


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var checkBoxes = []

# Called when the node enters the scene tree for the first time.
func _ready():
	var _err = connect("pressed", self, "ShowPopup")
	
	checkBoxes = $Popup/VBoxContainer.get_children()

	for b in checkBoxes:
		b.connect("toggled",self, "OnFilterChange")
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func ShowPopup():
	$Popup.popup()



func OnFilterChange():
	pass