extends Button


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var firstPause : bool = true

# Called when the node enters the scene tree for the first time.
func _ready():
	var _err = connect("toggled",self, "ButtonChangeState")
	$PlaySprite.hide()
	ButtonChangeState(true)
	pressed = true
func ButtonChangeState(isPressed: bool):

	if firstPause:
		firstPause = false
	else:
		Global.sfxPlayer.PlayPauseSound()


	if isPressed:
		SignalBus.emit_signal("PAUSE_ON")
	
		$PlaySprite.show()
		$PauseSprite.hide()

	if !isPressed:
		SignalBus.emit_signal("PAUSE_OFF")

		$PlaySprite.hide()
		$PauseSprite.show()

func _input(event):
	if Input.is_action_just_pressed("PAUSE"):
		return
		pressed = !pressed
		ButtonChangeState(pressed)
