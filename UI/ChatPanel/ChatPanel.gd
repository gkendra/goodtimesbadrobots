extends Panel


export (NodePath) var courierSelectorPath
export (NodePath) var statBoxParentPath
export (NodePath) var capacityContainerPath
export (NodePath) var statusDisplayPath
export (NodePath) var messageLogPath
export (NodePath) var portraitBoxPathBG
export (NodePath) var portraitBoxPathIMG

var currentCourier

var locateButton

#ailments:
var isLostTXT : String = "LOST, "
var isEncumberedTXT : String = "ENCUMBERED, "
var isApprehendedTXT : String = "IN CUSTODY, "
var isEnRouteTXT : String = "ENROUTE ,"
var isInBuildingTXT : String = "IN BUILDING ,"
var hasBadHabitsTXT : String = "Has bad habits..."
var respondingTXT : String = "Sending Message, "


func _ready():
	locateButton = find_node("LocateButton",true,false)

	call_deferred("ConnectSignals")
	
	
	var selector = find_node("CourierSelector",true,false)
	selector.InitCourierSelector()
	
	if 	!Global.main.activeGameData.couriers.empty():
		OnCourierChange(Global.main.activeGameData.couriers[0].id)



func ConnectSignals():
	var _err = get_node(courierSelectorPath).connect("CHANGED_COURIER",self, "OnCourierChange")

	_err = SignalBus.connect("COURIER_STATUS_CHANGED", self, "SignalUpdateStatusDisplay")

	_err = SignalBus.connect("COURIER_CAP_CHANGED", self, "SignalUpdateCarryingCapacityDisplay")

	_err = SignalBus.connect("COURIER_NEW_MSG", self, "SignalUpdateMessageLogDisplay")


	_err = locateButton.connect("LOCATE_PRESSED",self, "LocateCourier")


func OnCourierChange(newCourierID : int):
	print("changed")
	var myCourier 
	for c in Global.main.activeGameData.couriers:
		if c.id == newCourierID:
			myCourier = c
			currentCourier = c


	UpdateStatDisplay(myCourier)
	UpdateCarryingCapacityDisplay(myCourier)
	UpdateStatusDisplay(myCourier)
	UpdateMessageLogDisplay(myCourier)
	UpdatePortraitBoxDisplay(myCourier)



func UpdateStatDisplay(courier):
	var statPar = get_node(statBoxParentPath)
	var stats = statPar.get_children()
	for stat in stats:

		match (stat.type):
			(stat.STAT_TYPE.SPEED_STAT):
				stat.SetLabel("SPEED")
				stat.SetStat(courier.speed)

			(stat.STAT_TYPE.CAPACITY_STAT):
				stat.SetLabel("CAPACITY")
				stat.SetStat(courier.carryingCapacity)

			(stat.STAT_TYPE.COMMS_STAT):
				stat.SetLabel("COMMS.")
				stat.SetStat(courier.communicationSkills)

			(stat.STAT_TYPE.WISDOM_STAT):
				stat.SetLabel("WISDOM")
				stat.SetStat(courier.wisdom)
			


func UpdateCarryingCapacityDisplay(courier):
		get_node(capacityContainerPath).Update(courier)

func UpdateStatusDisplay(courier):
	print("updating status")
	var statDisplay = get_node(statusDisplayPath)

	var badHabits = ""
	var enRoute = ""
	var inBuilding = ""
	var lost = ""
	var encumbered = ""
	var apprehended = ""
	var sending = ""


	if courier.hasBadHabits: 
		badHabits = hasBadHabitsTXT
	if courier.isEnRoute: 
		enRoute = isEnRouteTXT
	if courier.isInBuilding:
		inBuilding = isInBuildingTXT
	if courier.isLost:
		lost = isLostTXT
	if courier.isEncumbered:
		encumbered = isEncumberedTXT
	if courier.isApprehended:
		apprehended = isApprehendedTXT
	if courier.isResponding:
		sending = respondingTXT

	statDisplay.text = badHabits + sending +  enRoute + inBuilding + lost + encumbered + apprehended
	print(badHabits + sending +  enRoute + inBuilding + lost + encumbered + apprehended)

	
func UpdateMessageLogDisplay(courier):
	pass

func UpdatePortraitBoxDisplay(courier):
	get_node(portraitBoxPathBG).color = courier.portraitBGColor
	get_node(portraitBoxPathIMG).texture = load(courier.portraitIndex)




#only update on Signal if displayed courier is same as changed courier
func SignalUpdateStatusDisplay(courierID : int):
	print("a")
	if currentCourier.id == courierID:
		UpdateStatusDisplay(currentCourier)
		print("b")

func SignalUpdateCarryingCapacityDisplay(courierID : int):
	if currentCourier.id == courierID:
		UpdateCarryingCapacityDisplay(currentCourier)

func SignalUpdateMessageLogDisplay(courierID : int):
	if currentCourier.id == courierID:
		UpdateMessageLogDisplay(currentCourier)


func LocateCourier():

	var courierWorldPos = Global.courierWorldManager.GetWorldCourierByID(currentCourier.id).global_position
	SignalBus.emit_signal("SHOW_COURIER_LOCATION", currentCourier, courierWorldPos)

