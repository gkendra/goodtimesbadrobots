extends OptionButton




signal CHANGED_COURIER

func _ready():
	pass

func InitCourierSelector():
	PopulateDropList()

func PopulateDropList():

	for c in Global.main.activeGameData.couriers:
			add_item(c.displayName,c.id)

func NewItemSelected(index : int):
	var itemID = get_item_id(index)
	print("aaaaaaaaa")

	emit_signal("CHANGED_COURIER", itemID)


func SetDisabled():
	disabled = true
