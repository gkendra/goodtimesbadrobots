extends HBoxContainer



export (Color) var encumberedColor

func _ready():
	pass 


func Update(courier):
	$MaxWeightLabel.text = str(courier.capacity)
	$CurrentWeightLabel.text = str(courier.currentCapacity)

	if courier.currentCapacity > courier.capacity:
		modulate = encumberedColor
	else:
		modulate = Color.white