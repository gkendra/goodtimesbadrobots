extends Control


var courier : CourierData
var isForHire : bool = false



export (NodePath) var courierDisplayNamePath
export (NodePath) var speedStatPath
export (NodePath) var capacityStatPath
export (NodePath) var commsStatPath
export (NodePath) var wisdomStatPath
export (NodePath) var portraitBGPath
export (NodePath) var portraitIMGPath
export (NodePath) var notesLabelPath
export (NodePath) var hireDismissButtonPath

export (NodePath) var costLabelPath
export (NodePath) var costPrefixPath
export (NodePath) var upKeepLabelPath
export (NodePath) var upKeepPrefixPath

export (NodePath) var statsBoxContainerPath


var badHabitsString = "Has bad habits."

var archetype : String



func _ready():
	var statsDisplays = get_node(statsBoxContainerPath).get_children()
	var _err = Global.main.activeGameData.connect("MONEY_CHANGED",self,"PreventSoftLock")
	for stat in statsDisplays:
		stat.connect("UPGRADE_CLICKED",self,"IncrementStat")


func InitPlanningBoardItem(newCourierData : CourierData, _isForHire : bool):
	courier = newCourierData
	isForHire = _isForHire
	
	call_deferred("Update")
	

func PreventSoftLock(money):

	if isForHire:
		return

	if money <= 100 && Global.main.activeGameData.couriers.size() == 1:
		get_node(hireDismissButtonPath).disabled = true
	else:
		get_node(hireDismissButtonPath).disabled = false

func Average(nums : Array):
	var total = 0
	for n in nums:
		total += n

	return (total/nums.size())

func Update():
	assert(courier != null, "no courier")
	archetype = courier.archetype
	get_node(courierDisplayNamePath).text = courier.displayName

	get_node(speedStatPath).SetStat(courier.speed)
	get_node(speedStatPath).SetLabel("Speed")

	get_node(capacityStatPath).SetStat(courier.carryingCapacity)
	get_node(capacityStatPath).SetLabel("Capacity")
	
	get_node(commsStatPath).SetStat(courier.communicationSkills)
	get_node(commsStatPath).SetLabel("Comms")

	get_node(wisdomStatPath).SetStat(courier.wisdom)
	get_node(wisdomStatPath).SetLabel("Wisdom")

	get_node(portraitBGPath).color = courier.portraitBGColor
	get_node(portraitIMGPath).texture = load(courier.portraitIndex)

	if isForHire:
		get_node(costLabelPath).text = "$" + str(courier.hireCost)
		get_node(upKeepLabelPath).text ="$" + str(courier.upKeep)
	else:
		get_node(costPrefixPath).hide()
		get_node(costLabelPath).hide()
		get_node(upKeepLabelPath).text ="$" + str(courier.upKeep)
		

	if !isForHire:
		get_node(speedStatPath).SetUpgradeCost(courier.GetStatUpgradeCost(0))
		get_node(capacityStatPath).SetUpgradeCost(courier.GetStatUpgradeCost(1))
		get_node(commsStatPath).SetUpgradeCost(courier.GetStatUpgradeCost(2))
		get_node(wisdomStatPath).SetUpgradeCost(courier.GetStatUpgradeCost(3))

	if isForHire:
		get_node(speedStatPath).HideButton()
		get_node(capacityStatPath).HideButton()
		get_node(commsStatPath).HideButton()
		get_node(wisdomStatPath).HideButton()

	
	if isForHire:
		get_node(hireDismissButtonPath).text = "HIRE"
	else:
		get_node(hireDismissButtonPath).text = "FIRE"


	if courier.hasBadHabits:
		get_node(notesLabelPath).text = badHabitsString
	else:
		get_node(notesLabelPath).text = ""


	PreventSoftLock(Global.main.activeGameData.money)


func HireDismissClicked():
	
	if isForHire:
		get_parent().get_parent().HireCourier(courier.archetype,courier.hireCost)
	else:
		get_parent().get_parent().DismissCourier(courier)

func IncrementStat(stat : int,statDisplay):
	var cost = courier.GetStatUpgradeCost(stat)
	if Global.main.activeGameData.AttemptToSpendMoney(cost):
		Global.main.activeGameData.ChargeExpenseToUpgrades(cost)
		var newStats = courier.IncrementStat(stat)
		statDisplay.SetStat(newStats[0])
		statDisplay.SetUpgradeCost(newStats[1])

	

