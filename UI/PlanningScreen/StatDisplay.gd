extends HBoxContainer

enum STAT_TYPE {SPEED_STAT, CAPACITY_STAT, COMMS_STAT, WISDOM_STAT}
export(STAT_TYPE) var type

signal UPGRADE_CLICKED
	
	# "speed"            : .9,
	# "carryingCapacity" : .25,
	# "commSkills"       : .1,
	# "wisdom"           : .8,

func _ready():
	pass # Replace with function body.




func SetStat(newNum : float):
	$ProgressBar.value = newNum
	if $ProgressBar.value >= 1:
		$ProgressBar.value = 1
		HideButton()

func SetLabel(newName : String):
	$Label.text = newName


func HideButton():
	if $Button != null :
		$Button.modulate = Color(1,1,1,0)
		$Button.disabled = true

func ShowButton():
	if $Button != null :
		$Button.modulate = Color(1,1,1,1)
		$Button.disabled = false

func SetUpgradeCost(newCost):
	$Button.text = "$" + str(newCost)


func OnButtonClick():
	emit_signal("UPGRADE_CLICKED",type, self)

