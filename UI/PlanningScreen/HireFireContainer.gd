extends HBoxContainer

export (NodePath) var courierBuilderPath

export (NodePath) var currentStaffBoardPath
export (NodePath) var forHireBoardPath

func _ready():
	pass 


func HireCourier(arch, cost):
	if Global.main.activeGameData.AttemptToSpendMoney(cost):
		get_node(courierBuilderPath).CreateCourier(arch, false)
		get_node(currentStaffBoardPath).UpdateBoard()

func DismissCourier(courier : CourierData):
	Global.main.activeGameData.DismissCourier(courier)
	get_node(currentStaffBoardPath).RemoveBoardItem(courier)
	Global.main.activeGameData.emit_signal("MONEY_CHANGED",Global.main.activeGameData.money)
