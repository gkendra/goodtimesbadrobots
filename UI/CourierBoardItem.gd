extends "res://UI/JobBoardItem.gd"



var isPick : bool = true


func _ready():
	pass 


func Update():
	.Update()
	

	var pickDrop = get_node(pickDropPath)
	if isPick:
		pickDrop.text = "PICK"

	else:
		pickDrop.text = "DROP"


	if get_node_or_null(locationPath) != null:
		var loc = ""
		if isPick:
			loc = "PICK " +  job.origin.displayName
		else:
			loc = "DROP " + job.destination.displayName
		get_node(locationPath).text = loc


func SetIsPick(val):
	isPick = val
	Update()



func MarkPickDropCompleted():
	if !isPick:
		#drop complete
		Global.main.activeGameData.AddJobToCompletedList(job)
		if job.isLate:
			Global.sfxPlayer.PlayDropLateSound()
		else:
			Global.sfxPlayer.PlayDropOnTimeSound()
	else:
		Global.sfxPlayer.PlayPickSound()

	queue_free()

func LockJob():
	pass
	

func OnMouseEntered():
	Global.jobManager.JobIsHovered(job,true,isPick)
