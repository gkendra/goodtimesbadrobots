extends OptionButton


var id : int = -1

signal REASSIGN

func _ready():
	pass

func InitDropList(parID : int):
	id = parID
	PopulateDropList()

func PopulateDropList():
	clear()
	add_item("Assign", -2)	

	if id != -1:
		add_item("NEW",999)	
	
	for c in Global.main.activeGameData.couriers:
		if c.id != id:
			add_item(c.displayName,c.id)

func NewItemSelected(index : int):
	var itemID = get_item_id(index)

	if itemID == -2:
		return
	print("sendto:" + str(itemID))
	emit_signal("REASSIGN", itemID)
	Global.sfxPlayer.PlaySwapSound()


func SetDisabled():
	disabled = true
