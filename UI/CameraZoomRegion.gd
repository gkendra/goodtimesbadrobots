extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	Global.cityCamera.zoomRegion = self


func OnMouseEnter():
	Global.cityCamera.SetHover(true)
	

func OnMouseExit():
	Global.cityCamera.SetHover(false)
	