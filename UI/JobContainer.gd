extends ScrollContainer



export var isCourier : bool = false
onready var vBox : VBoxContainer = $VBoxContainer

export var displayName = ""
var id : int = -1

var jobBoardItemSceneRef : PackedScene = preload("res://UI/JobBoardItem.tscn")
var courierBoardItemSceneRef : PackedScene = preload("res://UI/CourierBoardItem.tscn")

var boardChangeTimer : Timer
var boardChangeDelay = 0.1

signal BOARD_CHANGED

func _ready():
	UpdateName()
	InitTimer()
	var _err = SignalBus.connect("END_OF_DAY",self, "AddAllJobsToIncomplete")

func InitTimer():
	boardChangeTimer = Timer.new()
	add_child(boardChangeTimer)
	boardChangeTimer.wait_time = boardChangeDelay
	boardChangeTimer.one_shot = true
	boardChangeTimer.stop()
	var _err = boardChangeTimer.connect("timeout",self, "OnBoardChange")


func SetDisplayName(newName : String) -> void:
	displayName = newName
	UpdateName()

func AddNewJob(jobData : JobData):
	if !isCourier:
		var newJob = jobBoardItemSceneRef.instance()
		newJob.Init(jobData)
		vBox.add_child(newJob)
		

	if isCourier:
		var newDest = courierBoardItemSceneRef.instance()
		vBox.add_child(newDest)
		newDest.Init(jobData)
		newDest.SetIsPick(false)
			


		var newOrigin = courierBoardItemSceneRef.instance()
		vBox.add_child(newOrigin)
		newOrigin.Init(jobData)	
		newOrigin.SetIsPick(true)


		vBox.CheckIfDropIsBeforePick(newOrigin)
		
		

	
	

	UpdateName()
	boardChangeTimer.start()


func RemoveJobByID(jobId):
	var jobs = vBox.get_children()
	for j in jobs:
		if j.job.id == jobId:
			j.queue_free()

	boardChangeTimer.start()

func UpdateName():
	var isCourierDiv = 1

	if isCourier:
		isCourierDiv = 2

	name = displayName + " (" + str(ceil(vBox.get_child_count()/isCourierDiv)) + ")"


func GetNextJobLocation():
	if vBox.get_child_count() == 0:
		#no more jobs 
		return null

	var j = vBox.get_child(0)
	if j.isPick:
		return j.job.origin
	else:
		return j.job.destination


func OnBoardChange():
	if isCourier:
		UpdateName()
		emit_signal("BOARD_CHANGED", id)
	else:
		UpdateName()

func MarkJobAsComplete(_jobLocation : Node2D):
	var jobs = vBox.get_children()
	var pick = null
	var drop = null

	var jobID = vBox.get_child(0).job.id
	
	#print(jobID.origin.name)
	#print(jobID.destination.name)
	#print(jobLocation.name)
	#assert(jobID.origin.intersection == jobLocation || jobID.destination.intersection == jobLocation, "courier location does not match job location" )

	for j in jobs:
		if j.job.id == jobID:
			if j.isPick:
				pick = j
			
			if !j.isPick:
				drop = j

	if pick == null && drop != null:

		drop.MarkPickDropCompleted()
		
		OnBoardChange()

		var courier = Global.main.activeGameData.GetCourierByID(id)
		courier.RemoveWeight(drop.job.weight)

		
	elif pick != null && drop != null: #pick up
		
		pick.MarkPickDropCompleted()
		drop.RemoveAssignmentDroplist()
		OnBoardChange()

		var courier = Global.main.activeGameData.GetCourierByID(id)
		courier.AddWeight(pick.job.weight)

	
func AddAllJobsToIncomplete():
	var boards = vBox.get_children()
	if isCourier:
		for b in boards:
			if b.isPick == false:
				Global.main.activeGameData.incompleteJobs.append(b.job)
	else:
		for b in boards:
			Global.main.activeGameData.incompleteJobs.append(b.job)
