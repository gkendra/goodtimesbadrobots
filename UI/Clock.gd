extends Panel


var minutes :int = 0
var hours : int = 9

var timer : Timer

var dayEndHour : int = 17 # 5:00 clock, dont change this or it breaks
var noonHour = 12

var realTimePerMinute = .33 # in seconds

var dayIsOver : bool = false

func _ready():
	Global.clock = self
	timer = Timer.new()
	timer.one_shot = false
	timer.wait_time = realTimePerMinute
	var _err = timer.connect("timeout", self, "IncrementMinute")
	add_child(timer)
	timer.start()

	UpdateLabel()
	UpdateGlobalTime()

	_err     = SignalBus.connect("PAUSE_ON",self,"PauseOn")
	_err     = SignalBus.connect("PAUSE_OFF",self,"PauseOff")


func PauseOn():
	timer.paused = true

func PauseOff():
	timer.paused = false

func IncrementMinute():
	minutes += 1
	if minutes >59:
		minutes = 0
		hours += 1

		if hours == dayEndHour && !dayIsOver:
			dayIsOver = true
			SignalBus.emit_signal("PRE_END_OF_DAY")
			$DelayTimer.start()

		if hours > 23:
			hours = 0

	BroadcastCurrentDueTime()

	UpdateLabel()
	UpdateGlobalTime()

func UpdateLabel():
	var hourstxt = ""
	var minutestxt = ""

	if hours < 10:
		hourstxt = "0" + str(hours)
	else:
		hourstxt = str(hours)

	if minutes < 10:
		minutestxt = "0" + str(minutes)
	else:
		minutestxt = str(minutes)

	$Label.text = hourstxt + ":" + minutestxt

func GetDueTime(cHours :int, cMins: int) -> int:
	return  (cHours * 60) + cMins


func BroadcastCurrentDueTime():
	var due = GetDueTime(hours, minutes)
	SignalBus.emit_signal("CURRENT_TIME", due)

func GetStringTimeFromDueTime(dueTime : int) -> String:
	var _minutes = dueTime % 60
	var minString = str(_minutes)
	if _minutes == 0:
		minString = "00"
	if _minutes < 10:
		minString = "0" + str(_minutes)
	
	#warning_ignore:integer_divison
	var _hours = floor(dueTime/60)


	return str(_hours) + ":" + minString

func GetDayEndInDueTime():
	return dayEndHour * 60


func UpdateGlobalTime():
	Global.currentHour = hours
	Global.currentMinute = minutes
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func IsBeforeNoon() -> bool:
	if hours < 12:
		return true
	else:
		return false



func DelayTimerTimeout():
	SignalBus.emit_signal("END_OF_DAY")

