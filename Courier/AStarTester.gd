extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var moveSpeed : float = 200

var pathfinder : Node2D
var path = []



# Called when the node enters the scene tree for the first time.
func _ready():
	pathfinder = get_parent().get_parent()


func GetPathToPoint(toWorldPos):
	var start = global_position
	
	path = pathfinder.GetIntersectionPath(start,toWorldPos)
	
func _input(_event):
	if Input.is_action_just_pressed("LEFT_CLICK"):
		GetPathToPoint(get_global_mouse_position())


func _physics_process(delta):
	if path.empty() : 
		return
	var dest = path[0].global_position
	var dist = global_position.distance_to(dest)

	if dist < moveSpeed * delta:
		global_position = dest
		path.pop_front()

		if path.empty():
			return

	else:
		var moveDir = (path[0].global_position - global_position).normalized()

		global_position += moveDir * moveSpeed * delta





	
