extends Spatial


signal DEAD
signal HURT
signal HEALED
signal HEALTHCHANGED


var bloodSpray 

signal MAXHEALTH

enum BLOOD {HUMAN, CLOWN, ROBOT}

export var bloodType = BLOOD.HUMAN

export var maxHealth = 100
var currentHealth = 1



func _ready():
	Init()
	
func Init():
	currentHealth = maxHealth
	emit_signal("HEALTHCHANGED", currentHealth)

	emit_signal("MAXHEALTH", maxHealth)

	

func Hurt(damage:int):
	if damage == 0:
		return
	if currentHealth <= 0:
		return
		
	currentHealth -= damage
		
	if currentHealth <=0:
		emit_signal("DEAD")
	
	else:
		emit_signal("HURT")
		
	emit_signal("HEALTHCHANGED",currentHealth)

	
func Heal(amount:int):
	if currentHealth <= 0:
		return
		
	currentHealth += amount
	
	if currentHealth > maxHealth:
		currentHealth = maxHealth
		
	emit_signal("HEALED")
	emit_signal("HEALTHCHANGED", currentHealth)
	

	
