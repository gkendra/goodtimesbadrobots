extends Node


var displayName : String
var displayNumber : String

#stats
var statMin : float = 0.0
var starMax : float = 1.0

var speed : float
var trafficHandling : float
var carryingCapacity : float
var listeningSkills : float
var communicationSkills : float
var cityKnowledge : float 
var reliability : float



#habits
var hasBadHabits : bool
var isBadCommunicator : bool
var isBadListener : bool


