extends Node2D


var currentMoveSpeed : float = 40 #used to move, set to 0 when paused


var pathfinder : Node2D
var path = []
var currentDestination
var id setget SetCourierID, GetCourierID



var courier : CourierData

var pickDropTime = 3 #seconds, reduced by speed and wisdom


var courierWorldManager
var capChangeTimer : Timer

func _ready():
	$Polygon2D.hide()
	InitTimers()
	
	courierWorldManager = get_parent().get_parent()
	pathfinder = courierWorldManager.pathfinder

	var _err = SignalBus.connect("PAUSE_ON",self,"PauseOn")
	_err     = SignalBus.connect("PAUSE_OFF",self,"PauseOff")

	_err     = SignalBus.connect("COURIER_STATUS_CHANGED",self, "OnCapChange") 


func InitTimers():
	capChangeTimer = Timer.new()
	add_child(capChangeTimer)
	capChangeTimer.wait_time = 0.1
	capChangeTimer.one_shot = true
	capChangeTimer.autostart = false
	capChangeTimer.stop()
	var _err = capChangeTimer.connect("timeout",self,"RecalculateSpeed")

func _input(_event):
	if Input.is_action_just_pressed("show_couriers"):
		$Polygon2D.visible = !$Polygon2D.visible

func CalculateStats():

	currentMoveSpeed = courier.movementSpeed




	SetCourierColor()

func SetCourierData(newCourierData : CourierData):
	courier = newCourierData
	SetCourierID(newCourierData.id)
	CalculateStats()


	$PickDropTimer.wait_time = pickDropTime * (1 - courier.wisdom) + .05

func GetPathToPoint(toWorldPos):


	var start = global_position
	
	path = pathfinder.GetIntersectionPath(start,toWorldPos)
	

func PauseOn():
	currentMoveSpeed = 0
	$PickDropTimer.paused = true
	$LostTimer.paused = true

func PauseOff():
	currentMoveSpeed = courier.movementSpeed
	$PickDropTimer.paused = false
	$LostTimer.paused = false

func _physics_process(delta):
	if currentDestination == null: #no job
		
		return 

	
	if $PickDropTimer.is_stopped() == false:
		return

	if path.empty() && currentDestination != null : 
		if global_position.distance_to(currentDestination.global_position) < 50:
			ArrivedAtLocation()
		else:
			GetPathToPoint(currentDestination.global_position)
		

	if path.empty():
		
		return


	var dest = path[0].global_position
	var dist = global_position.distance_to(dest)

	if dist < currentMoveSpeed * delta:
		global_position = dest
		path.pop_front()

		if path.empty():
			ArrivedAtLocation()

	else:
		var moveDir = (path[0].global_position - global_position).normalized()

		if Global.main.activeGameData.isPaused == false:
			global_position += moveDir * currentMoveSpeed * delta

func ArrivedAtLocation():
	if $LostTimer.is_stopped() == false:
		return

	SetEnRoute(false)

	courierWorldManager.CourierArrived(id,currentDestination)
	path.clear()

	StartPickDropProcess()
	currentDestination = null



func GetNewDestination():
	currentDestination = null
	currentDestination = courierWorldManager.GetNextJobLocation(id)

	if currentDestination != null:
		GetPathToPoint(currentDestination.global_position)
		SetEnRoute(true)
	else:
		SetEnRoute(false)

		#But maybe you get lost
	if currentDestination != null:
		if !RollForRoutePlanning():
			var randomPos = pathfinder.GetRandomPointWorldPos()
			GetPathToPoint(randomPos)
			StartLost()

func SetCourierID(newId):
	id = newId
	

func GetCourierID():
	return id


func SetCourierColor():
	$Polygon2D.color = courier.portraitBGColor


func CourierBoardChanged():
	GetNewDestination()
	


func PickDropTimerTimeout():
	SetInBuilding(false)
	GetNewDestination()


func OnCapChange(someonesID):

	if someonesID == courier.id:
		capChangeTimer.start()
	

func RecalculateSpeed():
	currentMoveSpeed = courier.movementSpeed

func SetEnRoute(val):
	courier.isEnRoute = val
	OnWorldCourierStatusChanged()
	print("EN ROUTE")

func SetInBuilding(val):
	courier.isInBuilding = val
	OnWorldCourierStatusChanged()

func OnWorldCourierStatusChanged():
	SignalBus.emit_signal("COURIER_STATUS_CHANGED", courier.id)


func StartPickDropProcess():
	$PickDropTimer.start()
	SetInBuilding(true)


func StartLost():
	courier.isLost = true
	OnWorldCourierStatusChanged()
	$LostTimer.start()

func EndLost():
	courier.isLost = false
	OnWorldCourierStatusChanged()
	GetNewDestination()
	#IM Not lost anymore but I think i learned something!

func RollForRoutePlanning() -> bool:
	
	var roll1 = randf() - .05 #MAGIC NUMBER
	var roll2 = randf() - .05 
	var roll3 = randf() - .05  

	if (roll1 > courier.wisdom) && (roll2 > courier.wisdom) && (roll3 > courier.wisdom):
	#if (roll1 > courier.wisdom):
		return false

	else:
		return true
