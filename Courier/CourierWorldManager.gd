extends Node2D


var courierSceneRef : String = "res://Courier/CourierWorld.tscn"

var pathfinder
var jobBoardManager
var connectTimer : Timer

func _ready():
	Global.courierWorldManager = self
	pathfinder = get_parent()
	assert(pathfinder != null, "no pathfinder")

	SpawnCouriers()

	call_deferred("ConnectToJobBoard")


func ConnectToJobBoard():
	jobBoardManager = get_tree().get_root().find_node("JobBoard",true ,false)
	jobBoardManager.connect("BOARD_CHANGED", self, "JobBoardChanged")

func SpawnCouriers():
	var startingPoints = $StartingPoints.get_children()
	startingPoints.shuffle()


	var couriers = Global.main.activeGameData.couriers
	if couriers.empty():
		print("****NO COURIERS IN ACTIVE GAME***")
		return
	for c in couriers:
		var newC = load(courierSceneRef).instance()
		$CourierWorldContainer.add_child(newC)
		newC.SetCourierID(c.id)
		newC.SetCourierData(c)
		

		newC.global_position = startingPoints[0].global_position
		startingPoints.shuffle()


func JobBoardChanged(boardID):
	var children = $CourierWorldContainer.get_children()
	for c in children:
		if c.GetCourierID() == boardID:
			c.CourierBoardChanged()


func GetNextJobLocation(courierID):
	return jobBoardManager.GetNextCourierJobLocation(courierID)


func GetWorldCourierByID(checkID):
	var childs = $CourierWorldContainer.get_children()
	for child in childs:
		if child.courier.id == checkID:
			return child

func CourierArrived(courierID, currentLocation):
	jobBoardManager.MarkJobAsComplete(courierID, currentLocation)
