extends Control

var courierDataSceneRef = "res://Courier/CourierData.gd"

var courierTypes =  {"JUICE_BOX":
	{
	"displayName"       : "Juice Box",
	"portraitPath"      : "res://UI/Assets/Portraits/naive_bot_2px.png",
	"carryingCapacity"  : .1, 
	"speed"             : .35,
#	"listeningSkills"   : .8,
	"commSkills"        : .8,
	"wisdom"            : .2,
	"reliability"       : 1,
	"hasBadHabits"      : false,
	"hireCost"          : 100,
	"upKeep"            : 50,
	"archetype"         : "JUICE_BOX"
	},
	"DUMP_TRUCK":
	{
	"displayName"      : "Dump Truck",
	"portraitPath"     : "res://UI/Assets/Portraits/square_bot-2pxt.png",
	"carryingCapacity" : .85,
	"speed"            : .2,
#	"listeningSkills"  : .9,
	"commSkills"       : .1,
	"wisdom"           : .4,
	"reliability"      : 1,
	"hasBadHabits"     : false,	
	"hireCost"          : 210,
	"upKeep"            : 100,
	"archetype"         : "DUMP_TRUCK"
	},
	"LAZER":
	{
	"displayName"      : "Lazer",
	"portraitPath"     : "res://UI/Assets/Portraits/skinny_bot-2pxt.png",
	"carryingCapacity" : .45,
	"speed"            : .9,
	"listeningSkills"  : .3,
	"commSkills"       : .5,
	"wisdom"           : .65,
	"reliability"      : .5,
	"hasBadHabits"     : false,	
	"hireCost"          : 250,
	"upKeep"            : 50,
	"archetype"         : "LAZER"
	},
	"ZERO":
	{
	"displayName"      : "ZERO",
	"portraitPath"     : "res://UI/Assets/Portraits/zero_fixed.png",
	"carryingCapacity" : .4,
	"speed"            : .7,
	"listeningSkills"  : .8,
	"commSkills"       : .95,
	"wisdom"           : .95,
	"reliability"      : 1,
	"hasBadHabits"     : false,
	"hireCost"          : 300,
	"upKeep"            : 45,
	"archetype"         : "ZERO"	
	}
}



func _ready():
	pass 


func CreateCourier(courierType : String, isDummyCourier : bool):
	
	print("added courier")
	var courierData = load(courierDataSceneRef).new()  # <-need to create dummy couriers for hiring screen


	courierData.displayName  = courierTypes[courierType].displayName
	courierData.portraitIndex = courierTypes[courierType].portraitPath
	courierData.speed = courierTypes[courierType].speed
	#courierData.trafficHandling = courierTypes[courierType].
	courierData.carryingCapacity = courierTypes[courierType].carryingCapacity
#	courierData.listeningSkills = courierTypes[courierType].listeningSkills
	courierData.communicationSkills = courierTypes[courierType].commSkills
	courierData.wisdom  = courierTypes[courierType].wisdom
#	courierData.reliability = courierTypes[courierType].reliability
	courierData.hireCost= courierTypes[courierType].hireCost
	courierData.upKeep= courierTypes[courierType].upKeep
	courierData.hasBadHabits = courierTypes[courierType].hasBadHabits

	courierData.portraitBGColor = Global.GetRandomCourierColor()
	courierData.archetype = courierTypes[courierType].archetype
	print("ARCH " + courierData.archetype)

	courierData.capacity = courierData.carryingCapacity * courierData.maxCapacity
	courierData.movementSpeed = courierData.maxSpeed * courierData.speed

	if !isDummyCourier:
		courierData.id = Global.courierBuilderIDIndex
		Global.courierBuilderIDIndex += 1
	
		courierData.displayNumber  = String(courierData.id)
		courierData.displayName = courierData.displayName + "#" + courierData.displayNumber

		Global.main.activeGameData.AddCourier(courierData)

	return courierData
		




