extends Reference
class_name CourierData


var archetype : String
var displayName : String = "Big Bobba"
var displayNumber : String
var id : int 
var portraitIndex 
var portraitBGColor : Color

#stats

var speed : float #skill stat- 0 to 1
var maxSpeed : float = 270
var movementSpeed : float  #caluclate by multiplying max by speed stat

var carryingCapacity : float #skill stat- 0 to 1
var maxCapacity : float = 100
var currentCapacity : float = 0
var capacity : float #caluclate by multiplying max by speed stat

var communicationSkills : float
var wisdom : float
var hireCost : float
var upKeep : float

var hasBadHabits : bool

#ailments
var isLost : bool = false
var isEncumbered : bool = false
var isApprehended : bool = false
var isEnRoute : bool = false
var isInBuilding : bool = false
var isResponding : bool = false

var currentSpeedCost = 10
var currentCapacityCost = 10
var currentCommsCost = 10
var currentWisdomCost = 10


const statUpgradeIncrement = .05
const statUpgradeCostIncrease = 10


func _ready():
    var _err = SignalBus.connect("END_OF_DAY",self, "ResetWeight")

func IncrementStat(stat : int):
    print("SPEED" + str(speed))
    match (stat):
        (0):
            speed += statUpgradeIncrement
            currentSpeedCost += statUpgradeCostIncrease
            return [speed,currentSpeedCost]
		
        (1):
            carryingCapacity += statUpgradeIncrement
            currentCapacityCost  += statUpgradeCostIncrease
            return [carryingCapacity, currentCapacityCost]

        (2):
            communicationSkills += statUpgradeIncrement
            currentCommsCost  += statUpgradeCostIncrease
            return [communicationSkills, currentCommsCost]

        (3):
            wisdom += statUpgradeIncrement
            currentWisdomCost  += statUpgradeCostIncrease
            return [wisdom, currentWisdomCost]

    
func AddWeight(lbs):
    currentCapacity += lbs
    SignalBus.emit_signal("COURIER_CAP_CHANGED",id)
    CheckForEncumberance()

func RemoveWeight(lbs):
    currentCapacity -= lbs
    SignalBus.emit_signal("COURIER_CAP_CHANGED",id)
    CheckForEncumberance()


func ResetWeight():
    currentCapacity = 0

func CheckForEncumberance():
    if currentCapacity > capacity:
        isEncumbered = true
        movementSpeed = (speed * 0.5) * maxSpeed
        SignalBus.emit_signal("COURIER_STATUS_CHANGED",id)
        
    else:
        isEncumbered = false
        movementSpeed = speed  * maxSpeed
        SignalBus.emit_signal("COURIER_STATUS_CHANGED",id)


func GetStatUpgradeCost(statType : int):
    match(statType):
        (0):
            return currentSpeedCost
        (1):
            return currentCapacityCost
        (2):
            return currentCommsCost
        (3):
            return currentWisdomCost