extends Area2D


var neighbors : Array = []
var displayName : String = ""



func _ready():
	InitIntersection()

func AddNeighbor(newNeighbor : Area2D):
	neighbors.append(newNeighbor)
	$Polygon2D.color = Color.blanchedalmond


func InitIntersection():
	var names = get_tree().get_root().find_node("CompanyNames", true, false)
	displayName = Global.main.activeGameData.names.GetNewName()
	var d = $LocationsList.get_children()
	for dest in d:
		dest.displayName = displayName


func GetLocations():
	return $LocationsList.get_children()



