extends Node2D

var init : bool = false


var intersectionSceneRef : String = "res://City/Intersection.tscn"

signal CITYCOMPLETE

func _ready():
	pass
	

func SetInitalized():
	init = true


func BuildCityGrid():
	CreatePointsAtIntersections()
	CreatePointsAtEnds()
	
	call_deferred("SetInitalized")

func _physics_process(_delta):
	if init:

		var allStreets = $StreetsH.get_children() + $StreetsV.get_children()
	
		for h in allStreets:
			JoinPointsAlongStreet(h)

		emit_signal("CITYCOMPLETE")
		init = false

func JoinPointsAlongStreet(street : Line2D):
	var currentIntersection : Area2D = null
	var currentPoint = street.points[0] + street.global_position 
	var endPoint = street.points[1] + street.global_position
	
	currentPoint += currentPoint.direction_to(endPoint) * -50

	while currentPoint != endPoint:
		var spaceState = get_world_2d().direct_space_state
		var result = spaceState.intersect_ray(currentPoint, endPoint, [],1,false,true)
		if result:
			if currentIntersection != null:
				currentIntersection.AddNeighbor(result.collider )
				result.collider.AddNeighbor( currentIntersection )

			currentIntersection = result.collider
			currentPoint = currentIntersection.global_position 

		else:
			return
	

func CreatePointsAtIntersections():
	var streetsH = $StreetsH.get_children()
	var streetsV = $StreetsV.get_children()
	var coll 
	var aFrom
	var aTo
	var bFrom
	var bTo
	for h in streetsH:
		for v in streetsV:
			aFrom = h.points[0] + h.global_position  
			aTo = h.points[1] + h.global_position
			bFrom = v.points[0] + v.global_position
			bTo = v.points[1] + v.global_position

			coll = Geometry.segment_intersects_segment_2d(aFrom, aTo, bFrom, bTo)

			if coll:
				var inter = load(intersectionSceneRef).instance()
				$Intersections.add_child(inter)
				inter.global_position = coll
			


func CreatePointsAtEnds():
	var streetsH : Array = $StreetsH.get_children()
	var streetsV : Array = $StreetsV.get_children()
	var streetsAll = streetsH + streetsV 

	for h in streetsAll:
		var inter = load(intersectionSceneRef).instance()
		$Intersections.add_child(inter)
		inter.global_position = h.points[0] + h.global_position
		

		var inter2 = load(intersectionSceneRef).instance()
		$Intersections.add_child(inter2)
		inter2.global_position = h.points[1] + h.global_position
	





func GetLineIntersection(p0x : float, p0y : float, p1x : float, p1y : float, p2x : float, p2y : float, p3x : float, p3y : float):
	#returns vector 2 or null
	#some kind of math wizardy
	#https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
	var s1x : float = p1x - p0x
	var s1y : float = p1y - p0y
	var s2x : float = p3x - p2x
	var s2y : float = p3y - p2y

	var s : float
	var t : float

	s = (-s1y * (p0x - p2x) + s1x * (p0y - p2y)) / (-s2x * s1y + s1x * s2y)
	t = (s2x * (p0y - p2y) - s2y * (p0x - p2x) ) / (-s2x * s1y + s1x * s2y)

	if (s > 0 && s <= 1 && t >= 0 && t <= 1):
		#collision detected
		var result : Vector2 = Vector2.ZERO
		result.x = p0x + (t * s1x)
		result.y = p0y + (t * s1y)
		return result
	

	return null
	
