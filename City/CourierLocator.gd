extends Node2D

var currentCircleSize : float = 500

var currentMaxOffset : float = 500
var currentOffset : Vector2

var minCircleSize = 20

var circleColor : Color = Color.greenyellow
var circleAlpha : float = .5

var courier : CourierData

var originalPos : Vector2
var currentPos : Vector2

var lineWidth : float = 8

var phases = 5
var currentPhase = 0

var phaseTime :float = 1.5

var gamePaused : bool = false

func _ready():
	SignalBus.connect("PAUSE_ON",self,"PauseOn")
	SignalBus.connect("PAUSE_OFF",self,"PauseOff")


	gamePaused = Global.main.activeGameData.isPaused

	$Timer.wait_time = phaseTime


func InitLocator(_courier, worldPos):
	courier = _courier
	originalPos = worldPos


	circleColor = courier.portraitBGColor
	circleColor.a = circleAlpha

	ProcessPhase()
	

func ProcessPhase():
	if gamePaused && currentPhase != 0:
		return
	$AudioStreamPlayer.play()
	CalculateCircleSize()
	CalculateOffSet()
	SetCircleOffset()
	currentPos = originalPos + currentOffset
	$Timer.start()
	update()

func _draw():
	draw_circle(currentPos,currentCircleSize,circleColor)
	draw_empty_circle(currentPos,Vector2(0,currentCircleSize), Color.white,1)

func PauseOn():
	$Timer.paused = true
	gamePaused = true

func PauseOff():
	$Timer.paused = false
	gamePaused = false

	$Timer.start()



func OnTimerTimeout():
	currentPhase += 1
	if currentPhase < phases:
		ProcessPhase()
	else:
		EndSequence()
	
func EndSequence():
	#do longer destroy timer here or something
	queue_free()


func draw_empty_circle(circle_center : Vector2 ,circle_radius : Vector2 ,  color : Color,  resolution : int):
	#https://www.reddit.com/r/godot/comments/3ktq39/drawing_empty_circles_and_curves/
		var draw_counter = 1
		var line_origin = Vector2()
		var line_end = Vector2()
		line_origin = circle_radius + circle_center
	
		while draw_counter <= 360:
			line_end = circle_radius.rotated(deg2rad(draw_counter)) + circle_center
			draw_line(line_origin, line_end, color,lineWidth,true)
			draw_counter += 1 / resolution
			line_origin = line_end
	
		line_end = circle_radius.rotated(deg2rad(360)) + circle_center
		draw_line(line_origin, line_end, color,lineWidth,true)


func SetCircleOffset():
	var ang = rand_range(0,360)
	var dist = rand_range(0,currentMaxOffset)
	
	currentOffset = Vector2(dist,0).rotated(deg2rad(ang))


func CalculateOffSet():
	#currentMaxOffset = (1-(courier.communicationSkills/2)) * currentCircleSize 
	currentMaxOffset = currentCircleSize/2

func CalculateCircleSize(): #do this before offset
	currentCircleSize = (1-(courier.communicationSkills/2)) * currentCircleSize
	if currentCircleSize < minCircleSize:
		currentCircleSize = minCircleSize