extends Camera2D



var zoom_step = 1.1

var moveSpeedMax : float = 400.0
var moveAccel : float = 50.0
var currentVelocity : Vector2 = Vector2.ZERO
var friction : float = 0.9

var isHovered : bool = false

var zoomMin = .5
var zoomMax = 4

var zoomRegion : Control = null

var xBounds : Vector2 = Vector2(-1200,1500)
var yBounds : Vector2 = Vector2(-1500,1500)


func _ready():
	Global.cityCamera = self
	zoom = Vector2(3.93,3.93)
	global_position = Vector2(1012,499)

func SetHover(val):
	isHovered = val

func _input(event):
	if !isHovered:
		return	

	if event is InputEventMouse:
		if event.is_pressed() && ! event.is_echo():
			var mouse_position = event.position
			if event.button_index == BUTTON_WHEEL_DOWN:
				if zoom.x < zoomMax:
					zoom_at_point(zoom_step,mouse_position)
			else : if event.button_index == BUTTON_WHEEL_UP:
				if zoom.x > zoomMin:
					zoom_at_point(1/zoom_step,mouse_position)

		
func _physics_process(delta):
	MoveCamera(delta)

func MoveCamera(delta):
	currentVelocity = currentVelocity * friction

	var moveVec = Vector2.ZERO

	if Input.is_action_pressed("ui_up"):
		moveVec += Vector2.UP

	if Input.is_action_pressed("ui_down"):
		moveVec += Vector2.DOWN

	if Input.is_action_pressed("ui_left"):
		moveVec += Vector2.LEFT

	if Input.is_action_pressed("ui_right"):
		moveVec += Vector2.RIGHT
	
	currentVelocity += (moveVec * moveAccel)
	
	global_position += currentVelocity * delta
	global_position.x = clamp(global_position.x,xBounds.x,xBounds.y)
	global_position.y = clamp(global_position.y,yBounds.x,yBounds.y)

func zoom_at_point(zoom_change, point):
	var c0 = global_position # camera position
	#var v0 = get_viewport().size # vieport size
	var v0 = zoomRegion.rect_size # vieport size
	var c1 # next camera position
	var z0 = zoom # current zoom value
	var z1 = z0 * zoom_change # next zoom value

	c1 = c0 + (-0.5*v0 + point)*(z0 - z1)
	zoom = z1
	global_position = c1
	print(global_position)
