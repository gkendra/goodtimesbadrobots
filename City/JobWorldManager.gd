extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var jobWorldItemSceneRef : String = "res://Jobs/JobWorldItem.tscn"

var currentJob = null

onready var originFx = find_node("OriginFX",true,false)
onready var destFx = find_node("DestFX",true,false)

# Called when the node enters the scene tree for the first time.
func _ready():
	$Origin.hide()
	$Destination.hide()


func SetHoveredJob(job : JobData,isCourier : bool, isPick : bool):
	if job == null: 
		return 
	
	if !isCourier :
		originFx.hide()
		destFx.hide()

	if isCourier && isPick:
		originFx.show()
		destFx.hide()

	if isCourier && !isPick:
		originFx.hide()
		destFx.show()

	currentJob = job

	$Origin.show()
	$Origin.global_position = currentJob.origin.global_position

	$Destination.show()
	$Destination.global_position = currentJob.destination.global_position

	SetJobColor($Destination/Polygon2D, currentJob.type)
	SetJobColor($Origin/Polygon2D, currentJob.type)


func SetJobColor(target , jobType : int):
	
		match jobType:
			(0):
				target.self_modulate = Global.allDayColor
			(1):
				target.self_modulate = Global.standardColor
			(2):
				target.self_modulate = Global.expressColor
			(3):
				target.self_modulate = Global.priorityRushColor
	
