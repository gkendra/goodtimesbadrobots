extends Node2D



var grid = {}
var aStar : AStar2D

func _ready():
	var _err = $CityGridBuilder.connect("CITYCOMPLETE",self, "BuildAStarGrid")
	call_deferred("BuildCityGrid")

func BuildAStarGrid():
	aStar = AStar2D.new()
	MakeGrid()
	ConnectPoints()

	
func BuildCityGrid():
	$CityGridBuilder.BuildCityGrid()

func MakeGrid():
	var intersections = $CityGridBuilder/Intersections.get_children()

	grid = {}

	var id = 0
	for inter in intersections:
		
		grid[id] = {
			"intersection":inter,
			"neighbors" : inter.neighbors,
			"displayName" : inter.displayName
			}
			
		
		aStar.add_point(id, inter.global_position, 1.0)
		id += 1

func ConnectPoints():
	for id in grid:
		for n in grid[id].neighbors:
			aStar.connect_points(id,GetIDByIntersection(n) ,true)
		


func GetIDByIntersection(intersection : Area2D):

	for g in grid:
		if grid[g].intersection == intersection:
			return g

func FindClosestIDToWorldPos(worldPos : Vector2):
	var earlyExitDist : float = 10
	var closestDist : float = 8675309 #arbitrarily large number
	var closestID  = null
	
	for g in grid:
		var inter = grid[g].intersection
		var dist = worldPos.distance_to(inter.global_position)

		if dist <= earlyExitDist:
			return g

		if dist < closestDist:
			closestDist = dist
			closestID = g
	
	return closestID

func PrintAllConnections():
	for g in grid:
		var p = aStar.get_point_connections(g)
		print(p)

func GetRandomPointWorldPos():
	var r = int(rand_range(0,grid.size()))
	return grid[r].intersection.global_position
	
func GetIntersectionPath(startWorldPos : Vector2, endWorldPos : Vector2):

	var startID = FindClosestIDToWorldPos(startWorldPos)
	var endID = FindClosestIDToWorldPos(endWorldPos)
#	print("pathing from " + str(startID) + " to " + str(endID))
	if startID == endID:
		var empty = []
		return  empty

	var idPath = aStar.get_id_path(startID, endID)
	var interPath = []
	#print(idPath)
	for id in idPath:
		var inter = grid[id].intersection
		interPath.push_back(inter)

	return interPath
