extends Node2D

var courierLocationSceneRef = preload("res://City/CourierLocator.tscn")

func _ready():
	var _err = SignalBus.connect("SHOW_COURIER_LOCATION",self, "SpawnLocator")



func SpawnLocator(courier :CourierData, worldPos : Vector2):
	var cID = courier.id

	var childs = get_children()
	if !childs.empty():
		for c in childs:
			if c.courier.id == cID:
				return


	var loc = courierLocationSceneRef.instance()
	add_child(loc)
	loc.InitLocator(courier, worldPos)
