extends Node2D

var job : JobData = null

func _ready():
	pass # Replace with function body.

func InitJobWorldItem(newData : JobData):
	job = newData

	global_position = job.origin.global_position
	SetColor(job.type)

func SetColor(jobType : int):
		match jobType:
			(0):
				#$Polygon2D.self_modulate = Global.allDayColor
				$jobWorldItemIcon.self_modulate = Global.allDayColor
			(1):
			#	$Polygon2D.self_modulate = Global.standardColor
				$jobWorldItemIcon.self_modulate = Global.standardColor
			(2):
			#	$Polygon2D.self_modulate = Global.expressColor
				$jobWorldItemIcon.self_modulate = Global.expressColor
			(3):
			#	$Polygon2D.self_modulate = Global.priorityRushColor
				$jobWorldItemIcon.self_modulate = Global.priorityRushColor