extends Node2D




#Game States
var mainMenuSceneRef = "res://General/GameStates/MainMenu.tscn"
var planningSceneRef = "res://General/GameStates/Planning.tscn"
var gameSceneRef = "res://General/GameStates/Game.tscn"
var dayEndSceneRef = "res://General/GameStates/DayEnd.tscn"
var endGameResultsSceneRef = "res://General/GameStates/EndGameResults.tscn"


var activeGameDataSceneRef = "res://General/ActiveGameData.tscn"
var activeGameData = null
var currentStateObject

func _ready():
	activeGameData = $ActiveGameData
	Global.main = self
	PurgeCurrentState()
	ChangeToMainMenuState()

	



func AdvanceGameState(stateObject : Control, gameState : int):

	var curState =gameState
	
	#enum GAMESTATES {MAIN_MENU, PLANNING, GAME, DAY_END, GAME_END} 

	match (curState):
		stateObject.GAMESTATES.MAIN_MENU:
			#initalize new game
			CreateNewGameData()
			PurgeCurrentState()
			ChangeToPlanningState()
			

		stateObject.GAMESTATES.PLANNING:
			PurgeCurrentState()
			ChangeToGameState()

		stateObject.GAMESTATES.GAME:
			PurgeCurrentState()
			ChangeToDayEndState()

		stateObject.GAMESTATES.DAY_END:
			activeGameData.AdvanceDay()
			if activeGameData.IsGameOver():
				PurgeCurrentState()
				ChangeToEndGameState()
			else:
				PurgeCurrentState() #might want to keep game state here in the future as background?
				ChangeToPlanningState()

		stateObject.GAMESTATES.GAME_END:
			PurgeCurrentState()
			ChangeToMainMenuState()


	
	
	
func ChangeToMainMenuState():
	var s = load(mainMenuSceneRef).instance()
	add_child(s)
	currentStateObject = s
	
func ChangeToPlanningState():
	var s = load(planningSceneRef).instance()
	add_child(s)
	currentStateObject = s

func ChangeToGameState():
	var s = load(gameSceneRef).instance()
	add_child(s)
	currentStateObject = s

func ChangeToDayEndState():
	var s = load(dayEndSceneRef).instance()
	add_child(s)
	currentStateObject = s


func ChangeToEndGameState():
	var s = load(endGameResultsSceneRef).instance()
	add_child(s)
	currentStateObject = s

func PurgeCurrentState():
	if currentStateObject != null:
		currentStateObject.queue_free()

	

func CreateNewGameData():
	activeGameData.queue_free()
	var gameData = load(activeGameDataSceneRef).instance()
	add_child(gameData)
	activeGameData = gameData
