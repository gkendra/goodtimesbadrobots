extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
#warning-ignore:unused_signal
signal END_OF_DAY
#warning-ignore:unused_signal
signal PRE_END_OF_DAY
#warning-ignore:unused_signal
signal PAUSE_ON
#warning-ignore:unused_signal
signal PAUSE_OFF
#warning-ignore:unused_signal
signal CURRENT_TIME


#warning-ignore:unused_signal
signal COURIER_STATUS_CHANGED
#warning-ignore:unused_signal
signal COURIER_CAP_CHANGED
#warning-ignore:unused_signal
signal COURIER_NEW_MSG


#warning-ignore:unused_signal
signal WORLD_COURIER_STATUS_CHANGED
#warning-ignore:unused_signal
signal SHOW_COURIER_LOCATION

#warning-ignore:unused_signal
signal COURIER_ADDED