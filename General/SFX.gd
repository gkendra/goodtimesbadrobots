extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	Global.sfxPlayer = self

func PlayNegativeSound():
	$NegativeSound.play()


func PlayPauseSound():
	$PauseSound.play()

func PlayNotificationSound():
	$NotificationSound.play()

func PlaySwapSound():
	$SwapSound.play()

func PlayPickSound():
	$PickSound.play()

func PlayDropLateSound():
	$DropLateSound.play()

func PlayDropOnTimeSound():
	$DropOnTimeSound.play()