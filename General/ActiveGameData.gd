extends Node
class_name ActiveGameData


var couriers = []

var isPaused : bool = false

var startingMoney = 200
var money : float = 0

var totalEarningsByDay = []

var weekTotalJobCompletions = {
	"ALLDAY"      : 0,
	"STANDARD"    : 0,
	"EXPRESS"     : 0,
	"RUSH"        : 0,
	"INCOMPLETE"  : 0,
	"LATE"        : 0,
	"TOTAL_WEIGHT": 0,
	}

var weekTotalPayrollExpenses = 0
var weekTotalUpgradeExpenses = 0




var currentDay :int = 0
var maxDays : int = 1

var weightPayoutMultiplier : float = 2

var completedJobs = []
var incompleteJobs = []

signal MONEY_CHANGED
signal UPKEEP_CHANGED

var jobPayouts = {
	"ALLDAY" : 15,
	"STANDARD" : 30,
	"EXPRESS" : 60,
	"RUSH" : 120,
	"INCOMPLETE": 0,
	"LATE" : 5
}

var names

func _ready():
	InitEarnings()
	money = startingMoney
	names = $CompanyNames

	var _err = SignalBus.connect("PAUSE_ON",self, "PauseOn")
	_err = SignalBus.connect("PAUSE_OFF",self, "PauseOff")

func PauseOn():
	isPaused = true


func PauseOff():
	isPaused = false

func InitEarnings():
	totalEarningsByDay = [0,0,0,0,0]


func AdvanceDay():
	currentDay += 1
	completedJobs = []
	incompleteJobs = []
	print("****NEW DAY****" + str(currentDay))

func AddJobToCompletedList(newJob):
	completedJobs.push_back(newJob)


func IsGameOver() -> bool:
	if currentDay > maxDays:
		return true
	else:
		return false

func AddCourier(newCourier ):
	couriers.push_back(newCourier)
	emit_signal("UPKEEP_CHANGED", CalculateTotalUpKeep())
	SignalBus.emit_signal("COURIER_ADDED")

func DismissCourier(courier):
	couriers.erase(courier)
	emit_signal("UPKEEP_CHANGED", CalculateTotalUpKeep())

func GetCourierBoardByID(checkID):
	#HACK this function makes no sesne
	for c in couriers:
		if c.id == checkID:
			return c

func GetCourierByID(checkID):
	for c in couriers:
		if c.id == checkID:
			return c

func SetCurrentDayEarnings(newTotal):
	totalEarningsByDay[currentDay] = newTotal
	money += newTotal

func GetEarningsData():
	return totalEarningsByDay

func AddMoney(newMoney : float):
	money += newMoney
	emit_signal("MONEY_CHANGED", money)

func AttemptToSpendMoney(amnt : float) -> bool:
	if amnt <= money:
		money -= amnt
		emit_signal("MONEY_CHANGED", money)
		return true
	else:
		Global.sfxPlayer.PlayNegativeSound()
		return false


func ChargeExpenseToUpgrades(amount):
	weekTotalUpgradeExpenses += amount

func ChargeUpkeep():
	#this is called on Day End Screen when numbers are calculated for display
	money -= CalculateTotalUpKeep()
	emit_signal("MONEY_CHANGED", money)

	weekTotalPayrollExpenses += CalculateTotalUpKeep()

func CalculateTotalUpKeep():
	var total = 0
	for c in couriers:
		total += c.upKeep

	return total
