extends "res://General/GameStates/GameState.gd"


export(NodePath) var startButtonPath
export(NodePath) var tutorialButtonPath



# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	var _err = get_node(startButtonPath).connect("pressed",self,"OnStartPressed")
	_err = get_node(tutorialButtonPath).connect("pressed",self,"OnTutorialPressed")

func OnStartPressed():
	if !$AnimationPlayer.is_playing():
		$AnimationPlayer.play("Exit")
	

func ExitState():
	Global.main.AdvanceGameState(self, gameState)

func OnTutorialPressed():
	pass


func OnAnimFinish(_anim):
	if _anim == "Exit":
		ExitState()
