extends Button


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if Global.main.activeGameData.currentDay == 0:
		disabled = true

	var _err = SignalBus.connect("COURIER_ADDED",self, "AddedCourier")


func AddedCourier():
	disabled = false