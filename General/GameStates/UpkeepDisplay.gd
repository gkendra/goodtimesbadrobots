extends Label



func _ready():
	text =str(Global.main.activeGameData.CalculateTotalUpKeep() )
	Global.main.activeGameData.connect("UPKEEP_CHANGED",self, "UpdateLabel")

func UpdateLabel(newUpkeep):
	text = "$" + str(newUpkeep)