extends Label



func _ready():
	text =str(Global.main.activeGameData.money)
	Global.main.activeGameData.connect("MONEY_CHANGED",self, "UpdateLabel")

func UpdateLabel(newValue):
	text = ("$") + str(newValue) 