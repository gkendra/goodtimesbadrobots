extends  "res://General/GameStates/GameState.gd"


export(NodePath) var advanceButton

export (NodePath) var animPlayerPath
var animPlayer : AnimationPlayer

func _ready():
	var _err = get_node(advanceButton).connect("pressed",self, "OnAdvanceButtonClicked")
	animPlayer = get_node(animPlayerPath)
	animPlayer.play("Entry")

func OnAdvanceButtonClicked():
	animPlayer.play("Exit")


func AnimFinished(anim):
	if anim == "Exit":
		ExitState()

func ExitState():
	Global.main.AdvanceGameState(self,gameState)
