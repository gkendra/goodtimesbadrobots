extends  "res://General/GameStates/GameState.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (NodePath) var buttonPath

# Called when the node enters the scene tree for the first time.
func _ready():
	var _err = get_node(buttonPath).connect("pressed",self,"OnStartDayPressed")

	$CanvasLayer/AnimationPlayer.play("Entry")

func OnStartDayPressed():
	$CanvasLayer/AnimationPlayer.play("Exit")



func AnimFinished(anim):
	if anim == "Exit":
		ExitState()

func ExitState():
	Global.main.AdvanceGameState(self, gameState)
