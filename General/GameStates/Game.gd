extends "res://General/GameStates/GameState.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var _err = SignalBus.connect("END_OF_DAY",self, "OnDayEnd")


func OnDayEnd():
	call_deferred("EndDay")

func EndDay():
	Global.main.AdvanceGameState(self, gameState)
