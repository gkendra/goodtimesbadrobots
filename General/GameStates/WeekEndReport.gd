extends VBoxContainer

export (NodePath) var monPath
export (NodePath) var tuePath
export (NodePath) var wedPath
export (NodePath) var thuPath
export (NodePath) var friPath


export (NodePath) var allDayQuantityPath
export (NodePath) var standardQuantityPath
export (NodePath) var expressQuantityPath
export (NodePath) var priorityRushQuantityPath
export (NodePath) var totalWeightQuantityPath
export (NodePath) var incompleteQuantityPath
export (NodePath) var lateQuantityPath

export (NodePath) var allDayValuePath
export (NodePath) var standardValuePath
export (NodePath) var expressValuePath
export (NodePath) var priorityRushValuePath
export (NodePath) var totalWeightValuePath
export (NodePath) var incompleteValuePath
export (NodePath) var lateValuePath

export (NodePath) var allDayTotalCashPath
export (NodePath) var standardTotalCashPath
export (NodePath) var expressTotalCashPath
export (NodePath) var priorityRushTotalCashPath
export (NodePath) var totalWeightTotalCashPath
export (NodePath) var incompleteTotalCashPath
export (NodePath) var lateTotalCashPath

export (NodePath) var grandTotalPath
export (NodePath) var payrollExpensesPath
export (NodePath) var cashOnHandPath

var payrollExpensesPrefix : String = "PAYROLL EXPENSES: -"
var cashOnHandPrefix : String = "FINAL CASH ON HAND: $"

var allDayQty : int = 0
var standardQty : int  = 0
var expressQty : int  = 0
var rushQty : int  = 0
var totalWeightQty : int  = 0
var lateJobsQty : int  = 0
var incompleteJobsQty : int  = 0

var allDayCash : int = 0
var standardCash : int = 0
var expressCash : int = 0
var rushCash : int = 0
var totalWeightCash : int = 0
var lateJobsCash : int = 0
var incompleteJobsCash : int = 0

var totalDayEarnings : float = 0



func _ready():
	$AnimationPlayer.play("Entry")

func Update():
	GetJobsQuantity()
	CalculateCashEarnings()
	CalculateTotalEarnings()
	
	DisplayJobsQuantity()
	DisplayJobsValueEach()
	DisplayCashEarnings()
	DisplayDayTotalEarnings()
	DisplayPreviousDaysEarnings()
	DisplayPayrollExpenses()
	DisplayCashOnHand()
		
func DisplayJobsValueEach():
	get_node(allDayValuePath).text = "$" + str(Global.main.activeGameData.jobPayouts.ALLDAY)
	get_node(standardValuePath).text = "$" +  str(Global.main.activeGameData.jobPayouts.STANDARD)
	get_node(expressValuePath).text = "$" +  str(Global.main.activeGameData.jobPayouts.EXPRESS)
	get_node(priorityRushValuePath).text = "$" +  str(Global.main.activeGameData.jobPayouts.RUSH)
	get_node(totalWeightValuePath).text = "$" + str(Global.main.activeGameData.weightPayoutMultiplier)
	get_node(incompleteValuePath).text = "$" + str(Global.main.activeGameData.jobPayouts.INCOMPLETE)
	get_node(lateValuePath).text = "$" + str(Global.main.activeGameData.jobPayouts.LATE)

func GetJobsQuantity():

	totalWeightQty    = Global.main.activeGameData.weekTotalJobCompletions["TOTAL_WEIGHT"]
	lateJobsQty       = Global.main.activeGameData.weekTotalJobCompletions["LATE"]
	allDayQty         = Global.main.activeGameData.weekTotalJobCompletions["ALLDAY"]
	standardQty       = Global.main.activeGameData.weekTotalJobCompletions["STANDARD"]
	expressQty        = Global.main.activeGameData.weekTotalJobCompletions["EXPRESS"]
	rushQty           = Global.main.activeGameData.weekTotalJobCompletions["RUSH"]
	incompleteJobsQty = Global.main.activeGameData.weekTotalJobCompletions["INCOMPLETE"]


func DisplayJobsQuantity():
	get_node(allDayQuantityPath).text = str(allDayQty)
	get_node(standardQuantityPath).text = str(standardQty)
	get_node(expressQuantityPath).text = str(expressQty)
	get_node(priorityRushQuantityPath).text = str(rushQty)
	get_node(totalWeightQuantityPath).text = str(totalWeightQty)
	get_node(incompleteQuantityPath).text = str(incompleteJobsQty)
	get_node(lateQuantityPath).text = str(lateJobsQty)


func CalculateCashEarnings():
	allDayCash = Global.main.activeGameData.jobPayouts.ALLDAY * allDayQty
	standardCash = Global.main.activeGameData.jobPayouts.STANDARD * standardQty
	expressCash = Global.main.activeGameData.jobPayouts.EXPRESS * expressQty
	rushCash = Global.main.activeGameData.jobPayouts.RUSH * rushQty
	totalWeightCash  = totalWeightQty * Global.main.activeGameData.weightPayoutMultiplier
	lateJobsCash  = Global.main.activeGameData.jobPayouts.LATE * lateJobsQty
	incompleteJobsCash  = Global.main.activeGameData.jobPayouts.INCOMPLETE	* lateJobsQty

func DisplayCashEarnings():
	get_node(allDayTotalCashPath).text = "$" +  str(allDayCash)
	get_node(standardTotalCashPath).text = "$" +  str(standardCash)
	get_node(expressTotalCashPath).text = "$" +  str(expressCash)
	get_node(priorityRushTotalCashPath).text = "$" +  str(rushCash)
	get_node(totalWeightTotalCashPath).text = "$" +  str(totalWeightCash)
	get_node(lateTotalCashPath).text = "$" +  str(lateJobsCash)
	get_node(incompleteTotalCashPath).text = "$" +  str(incompleteJobsCash)


func CalculateTotalEarnings():
	var weekEarnings = Global.main.activeGameData.GetEarningsData()
	var ssum = 0

	for day in weekEarnings:
		ssum += day

	totalDayEarnings = ssum
	
#	totalDayEarnings = allDayCash + standardCash + expressCash + rushCash + totalWeightCash + lateJobsCash + incompleteJobsCash


func DisplayDayTotalEarnings():
	get_node(grandTotalPath).text = "TOTAL EARNINGS: " + str(totalDayEarnings)
	

func DisplayPayrollExpenses():
	var expense = Global.main.activeGameData.weekTotalPayrollExpenses
	get_node(payrollExpensesPath).text = payrollExpensesPrefix + str(expense)
	


func DisplayCashOnHand():
	get_node(cashOnHandPath).text = cashOnHandPrefix +  str(Global.main.activeGameData.money)

func DisplayPreviousDaysEarnings():
	var earnings = Global.main.activeGameData.totalEarningsByDay
	get_node(monPath).text = str(earnings[0])
	get_node(tuePath).text = str(earnings[1])
	get_node(wedPath).text = str(earnings[2])
	get_node(thuPath).text = str(earnings[3])
	get_node(friPath).text = str(earnings[4])
