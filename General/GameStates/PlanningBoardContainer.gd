extends ScrollContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var planningBoardItemSceneRef = "res://UI/PlanningBoardItem.tscn"
export (NodePath) var courierBuilderPath

export var isForHireBoard : bool

var archetypesToLoad = ["JUICE_BOX", "DUMP_TRUCK", "LAZER", "ZERO"]

# Called when the node enters the scene tree for the first time.
func _ready():
	UpdateBoard()

func UpdateBoard():
	ClearBoard()
	if isForHireBoard:
		PopulateBoardWithDummys()
	else:
		PopulateBoardWithStaff()

func ClearBoard():
	var boxes = $VBoxContainer.get_children()
	for b in boxes:
		b.queue_free()

func PopulateBoardWithDummys():
	for arch in archetypesToLoad:
		var item = load(planningBoardItemSceneRef).instance()
		$VBoxContainer.add_child(item)

		var cor = get_node(courierBuilderPath).CreateCourier(arch, true)
		print(cor)
		item.InitPlanningBoardItem(cor,true)


func PopulateBoardWithStaff():
	for curData in Global.main.activeGameData.couriers:
		var item = load(planningBoardItemSceneRef).instance()
		$VBoxContainer.add_child(item)
		
		item.InitPlanningBoardItem(curData,false)

func HireCourier(arch : String, cost : float):
	get_parent().HireCourier(arch, cost)


func DismissCourier(courier):
	get_parent().DismissCourier(courier)


func RemoveBoardItem(courier):
	var boxes = $VBoxContainer.get_children()
	for b in boxes:
		if b.courier == courier:
			b.queue_free()
