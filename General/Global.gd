extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var jobGenerator = null
var cityCamera : Camera2D = null

var currentHour : int = 0
var currentMinute : int = 0

var jobManager = null
var jobBoardManager = null
var courierWorldManager = null
var clock = null

var courierBuilderIDIndex : int = 0

var main

var sfxPlayer 

var allDayColor : Color = Color(0, 0.176471, 0.427451)
var standardColor : Color =  Color(0.015686, 0.541176, 0.701961)
var expressColor: Color = Color(0.721569, 0.372549, 0.094118)
var priorityRushColor : Color = Color(0.803922, 0, 0)

var colorIndex = 0

var courierColors = [
	
	Color("#722D06"),
	Color("#156C61"),
	Color("#22718F"),
	Color("#A5D552"),
	Color("#CA9F2C"),
	Color("#3D5B2E"),
	Color("#9D405E"),
	Color("#8F472F"),
	Color("#777611"),
	Color("#3B9EAF"),
	Color("#4a9422"),
]



func GetRandomCourierColor() -> Color:
	colorIndex += 1
	if colorIndex > courierColors.size() -1:
		colorIndex = 0

	return courierColors[colorIndex]

# Called when the node enters the scene tree for the first time.
func _ready():
	courierColors.shuffle()


